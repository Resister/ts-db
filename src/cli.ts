import PairUps from "./domain";
import { PairClient, PokemonClient, StarwarsClient } from "./clients";

async function main(): Promise<void> {
  const pairUps = new PairUps();
  await pairUps.init({
    pairClient: new PairClient(),
    starwarsClient: new StarwarsClient(),
    pokemonClient: new PokemonClient(),
  });
  await pairUps.create({ pokemonId: "UG9rZW1vbjowMDE=", starwarsId: "1" });
  const pair = await pairUps.get(1);
  console.log({ pair });
}

main().then();
