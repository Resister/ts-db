import { createConnection, Repository } from "typeorm";
import { get } from "request-promise";
import {
  Pair,
  ReadOnlyResource,
  ReadOnlyClientBase,
  ClientBase,
} from "./models";
import { PairEntity, PokemonEntity, StarwarsEntity } from "./entities";
import { GQLClient } from "./utils";

export class PairClient extends ClientBase {
  private pairRepo!: Repository<PairEntity>;

  private pokemonRepo!: Repository<PokemonEntity>;

  private starwarsRepo!: Repository<StarwarsEntity>;

  async init(): Promise<void> {
    const conn = await createConnection({
      type: "sqlite",
      entities: [PokemonEntity, StarwarsEntity, PairEntity],
      database: "database.sqlite",
      synchronize: true,
      logging: false,
    });
    this.pokemonRepo = conn.getRepository(PokemonEntity);
    this.starwarsRepo = conn.getRepository(StarwarsEntity);
    this.pairRepo = conn.getRepository(PairEntity);
  }

  async create(pair: Pair): Promise<Pair> {
    const { name, pokemon, starwarsChar } = pair;

    const pokemonEntity = new PokemonEntity();
    const starwarsEntity = new StarwarsEntity();
    pokemonEntity.name = pokemon!.name;
    pokemonEntity.id = pokemon!.id;
    if (!(await this.pokemonRepo.findOne(pokemon!.id))) {
      await this.pokemonRepo.save(pokemonEntity);
    }
    starwarsEntity.name = starwarsChar!.name;
    starwarsEntity.id = starwarsChar!.id;
    if (!(await this.starwarsRepo.findOne(starwarsChar!.id))) {
      await this.starwarsRepo.save(starwarsEntity);
    }
    const pairEntity = new PairEntity();
    pairEntity.name = name;
    pairEntity.pokemonEntity = pokemonEntity;
    pairEntity.starwarsEntity = starwarsEntity;
    await this.pairRepo.save(pairEntity);
    return pair;
  }

  async get(pairId: number): Promise<Pair> {
    const pairEntity = (await this.pairRepo.findOne(pairId, {
      relations: ["pokemonEntity", "starwarsEntity"],
    })) as PairEntity;
    return {
      id: pairEntity.id,
      name: pairEntity.name,
      pokemon: pairEntity.pokemonEntity,
      starwarsChar: pairEntity.starwarsEntity,
    };
  }
}
interface PokemonData {
  pokemon: ReadOnlyResource;
}
export class PokemonClient extends ReadOnlyClientBase {
  private gqlClient: GQLClient;

  constructor() {
    super();
    this.gqlClient = new GQLClient("https://graphql-pokemon2.vercel.app/");
  }

  async get(pokemonId: string): Promise<ReadOnlyResource> {
    const { pokemon } = await this.gqlClient.exec<PokemonData>(
      `{pokemon(id:"${pokemonId}"){id name}}`
    );
    return pokemon;
  }
}

export class StarwarsClient extends ReadOnlyClientBase {
  private readonly url: string;

  constructor() {
    super();
    this.url = "https://swapi-api.hbtn.io/api/people";
  }

  async get(starwarsId: string): Promise<ReadOnlyResource> {
    const response = await get(`${this.url}/${starwarsId}/`, { json: true });
    return { id: starwarsId, ...response };
  }
}
