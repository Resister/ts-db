import "reflect-metadata";
import { deepStrictEqual as assert } from "assert";
import {
  Pair,
  ClientBase,
  ReadOnlyClientBase,
  ReadOnlyResource,
} from "./models";
import PairUps from "./domain";

const POKEMON_NAME = "Bulbasaur";
const STARWARS_NAME = "Luke Skywalker";
const ID = "1";

class MockClient extends ClientBase {
  private pairs: Pair[];

  constructor() {
    super();
    this.pairs = [];
  }

  // eslint-disable-next-line class-methods-use-this
  async init(): Promise<void> {
    return Promise.resolve();
  }

  async get(pairId: number): Promise<Pair> {
    const result = this.pairs.find((pair) => pair.id === pairId) as Pair;
    return Promise.resolve(result);
  }

  async create(pair: Pair): Promise<Pair> {
    const updatedResource = { id: 1, ...pair };
    this.pairs.push(updatedResource as Pair);
    return Promise.resolve(updatedResource as Pair);
  }
}

class MockReadOnlyClient extends ReadOnlyClientBase {
  private resources: ReadOnlyResource[];

  constructor() {
    super();
    this.resources = [];
  }

  async get(resourceId: string): Promise<ReadOnlyResource> {
    const result = this.resources.find(
      (resource) => resource.id === resourceId
    ) as ReadOnlyResource;
    return Promise.resolve(result);
  }

  async create(resource: ReadOnlyResource): Promise<ReadOnlyResource> {
    const updatedResource = { ...resource, id: ID };
    this.resources.push(updatedResource);
    return Promise.resolve(updatedResource);
  }
}

const pairClient = new MockClient();
const starwarsClient = new MockReadOnlyClient();
const pokemonClient = new MockReadOnlyClient();

const pairUps = new PairUps();

// eslint-disable-next-line max-lines-per-function
describe("Pair Ups", () => {
  let actual: Pair;
  let expected: Pair;
  before("pair ups client", async () => {
    await pokemonClient.create({ id: "1", name: POKEMON_NAME });
    await starwarsClient.create({ id: "1", name: STARWARS_NAME });
    await pairUps.init({
      pairClient,
      pokemonClient,
      starwarsClient,
    });
  });
  before("expected", () => {
    expected = {
      id: Number(ID),
      name: `${STARWARS_NAME} and ${POKEMON_NAME}`,
      pokemon: { id: ID, name: POKEMON_NAME },
      starwarsChar: { id: ID, name: STARWARS_NAME },
    };
  });
  it("creates a pair", async () => {
    actual = await pairUps.create({
      pokemonId: ID,
      starwarsId: ID,
    });
    assert(actual, expected);
  });
  it("gets a pair", async () => {
    actual = await pairUps.get(1);
    assert(actual, expected);
  });
});
