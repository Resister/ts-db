import { Pair, ReadOnlyClientBase, ClientBase } from "./models";

export interface PairUpProps<
  C extends ClientBase,
  R extends ReadOnlyClientBase
> {
  pairClient: C;
  pokemonClient: R;
  starwarsClient: R;
}

export default class PairUps<
  C extends ClientBase,
  R extends ReadOnlyClientBase
> {
  private pairClient!: C;

  private pokemonClient!: R;

  private starwarsClient!: R;

  async init(props: PairUpProps<C, R>): Promise<void> {
    this.pairClient = props.pairClient;
    this.pokemonClient = props.pokemonClient;
    this.starwarsClient = props.starwarsClient;
    await this.pairClient.init();
  }

  async create({
    pokemonId,
    starwarsId,
  }: {
    pokemonId: string;
    starwarsId: string;
  }): Promise<Pair> {
    const starwarsChar = await this.starwarsClient.get(starwarsId);
    const pokemon = await this.pokemonClient.get(pokemonId);
    const name = `${starwarsChar.name} and ${pokemon.name}!`;
    const pair: Pair = { name, pokemon, starwarsChar };
    return this.pairClient.create(pair);
  }

  async get(pairId: number): Promise<Pair> {
    return this.pairClient.get(pairId);
  }
}
