import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  PrimaryColumn,
  ManyToOne,
} from "typeorm";

@Entity()
export class PokemonEntity {
  @PrimaryColumn()
  id!: string;

  @Column()
  name!: string;
}

@Entity()
export class StarwarsEntity {
  @PrimaryColumn()
  id!: string;

  @Column()
  name!: string;
}

@Entity()
export class PairEntity {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column()
  name!: string;

  @ManyToOne(() => PokemonEntity, (p) => p.id)
  pokemonEntity!: PokemonEntity;

  @ManyToOne(() => StarwarsEntity, (s) => s.id)
  starwarsEntity!: StarwarsEntity;
}
