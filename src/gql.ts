import "reflect-metadata";
import express from "express";
import * as Sentry from "@sentry/node";
import { graphqlHTTP } from "express-graphql";
import {
  Arg,
  buildSchemaSync,
  Ctx,
  Mutation,
  Query,
  Resolver,
} from "type-graphql";
import { config, setupLogger } from "./utils";
import { ClientBase, Pair, ReadOnlyClientBase } from "./models";
import PairUps from "./domain";
import { PairClient, PokemonClient, StarwarsClient } from "./clients";

const logger = setupLogger(config);
const app = express();

@Resolver()
class PairResolver {
  @Query(() => Pair)
  async team(
    @Ctx() pairUps: PairUps<ClientBase, ReadOnlyClientBase>
  ): Promise<Pair> {
    return pairUps.get(1);
  }

  @Mutation(() => Pair)
  async createTeam(
    @Arg("pokemonId") pokemonId: string,
    @Arg("starwarsId") starwarsId: string,
    @Ctx() pairUps: PairUps<ClientBase, ReadOnlyClientBase>
  ): Promise<Pair> {
    return pairUps.create({ pokemonId, starwarsId });
  }
}

const pairUps = new PairUps();
(async (): Promise<void> => {
  await pairUps.init({
    pairClient: new PairClient(),
    starwarsClient: new StarwarsClient(),
    pokemonClient: new PokemonClient(),
  });
})();
const schema = buildSchemaSync({
  resolvers: [PairResolver],
});
app.use("/graphql", graphqlHTTP({ schema, context: pairUps }));

app.listen(config.port, config.host);
Sentry.init({
  dsn: config.sentryUrl,
});
logger.info(`Running on ${config.host}:${config.port}`);

export default app;
