import { Field, ObjectType } from "type-graphql";

@ObjectType()
export class ReadOnlyResource {
  @Field()
  name!: string;

  @Field()
  id!: string;
}

@ObjectType()
export class Pair {
  @Field()
  name!: string;

  @Field()
  id?: number;

  @Field(() => ReadOnlyResource)
  pokemon?: ReadOnlyResource;

  @Field(() => ReadOnlyResource)
  starwarsChar?: ReadOnlyResource;
}
export abstract class ReadOnlyClientBase {
  abstract get(resource_id: string): Promise<ReadOnlyResource>;
}

export abstract class ClientBase {
  abstract init(): Promise<void>;

  abstract get(pair_id: number): Promise<Pair>;

  abstract create(pair: Pair): Promise<Pair>;
}
