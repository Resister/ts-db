import * as winston from "winston";
import { post } from "request-promise";
import { z } from "zod";

type Parseable = string | boolean | number;
const _ = typeof 0;
type TypeOf = typeof _;

export interface GQLResponse<T> {
  data: T;
}

function parse(key: string, typeArg: TypeOf): Parseable {
  if (typeArg === "number") {
    return JSON.parse(key);
  }
  if (typeArg === "boolean") {
    return JSON.parse(key.toLowerCase());
  }
  if (typeArg === "string") {
    return key;
  }
  throw new Error("Can only parse boolean, number and strings");
}

export class GQLClient {
  url: string;

  constructor(url: string) {
    this.url = url;
  }

  async exec<T>(query: string): Promise<T> {
    // eslint-disable-next-line security/detect-unsafe-regex
    const match = query.match(/(?<={)\w+/);
    if (!match) {
      throw new Error("Malformed Query");
    }
    const response: GQLResponse<T> = await post(this.url, {
      body: {
        query,
      },
      json: true,
    });
    return response.data;
  }
}

interface GetVarOpts {
  key: string;
  _default?: Parseable;
  typeArg?: TypeOf;
}

export function getVar({ key, _default, typeArg }: GetVarOpts): Parseable {
  const res = process.env[key];
  if (res && _default) {
    return parse(res, typeof _default);
  }
  if (res && typeArg) {
    return parse(res, typeArg);
  }
  if (!res && _default) {
    return _default;
  }
  if (!_default && !typeArg) {
    throw new Error("Either typeArg or _default required");
  }
  throw new Error(`Environment Variable ${key} required`);
}

const Config = z.object({
  logLevel: z.union([
    z.literal("ERROR"),
    z.literal("WARN"),
    z.literal("INFO"),
    z.literal("DEBUG"),
  ]),
  sentryUrl: z.string().url(),
  port: z.number(),
  host: z.string(),
  e2e: z.number(),
});

export const config = Config.parse({
  logLevel: getVar({ key: "LOG_LEVEL", _default: "DEBUG" }),
  sentryUrl: getVar({ key: "SENTRY_URL", typeArg: "string" }),
  port: getVar({ key: "PORT", _default: 3001 }),
  host: getVar({ key: "HOST", _default: "127.0.0.1" }),
  e2e: getVar({ key: "E2E", typeArg: "number" }),
});

export function setupLogger(cfg: z.infer<typeof Config>): winston.Logger {
  const level = cfg.logLevel.toLowerCase();
  switch (level) {
    case "error":
    case "warn":
    case "info":
    case "debug":
      break;
    default:
      throw new Error(
        "Logger only accepts ERROR, WARN, INFO, and DEBUG levels"
      );
  }
  return winston.createLogger({
    transports: [
      new winston.transports.Console({
        level,
        format: winston.format.printf(
          ({ level: lvl, message }): string =>
            `${lvl.toUpperCase()}: ${message}`
        ),
      }),
    ],
  });
}
