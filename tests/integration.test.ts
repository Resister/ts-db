import { deepStrictEqual as assert } from "assert";
import supertest from "supertest";
import nock from "nock";
import app from "../src/gql";
import { config } from "../src/utils";

const PIKA_ID = "UG9rZW1vbjowMjU=";
let STARWARS_NAME = "Luke Skywalker";
let POKEMON_NAME = "Pikachu";

if (!config.e2e) {
  STARWARS_NAME = "Joe";
  POKEMON_NAME = "MockPokemon";

  nock("https://swapi-api.hbtn.io/api/people/1")
    .get("/")
    .reply(200, { name: STARWARS_NAME });

  nock("https://graphql-pokemon2.vercel.app")
    .post("/", { query: /.+/i })
    .reply(
      200,
      JSON.stringify({
        data: { pokemon: { name: POKEMON_NAME, id: PIKA_ID } },
      })
    );
}

it("creates a team", async () => {
  const query = /* GraphQL */ `mutation {
      createTeam(pokemonId: "${PIKA_ID}", starwarsId: "1"){
        name
      }
    }`;
  const res = await supertest(app).post("/graphql").send({ query });
  assert(res.body.errors, undefined);
  assert(
    res.body.data.createTeam.name,
    `${STARWARS_NAME} and ${POKEMON_NAME}!`
  );
});
